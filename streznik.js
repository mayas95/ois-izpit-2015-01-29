var express = require('express'), path = require('path'), fs = require('fs');

var app = express();
app.use(express.static(__dirname + '/public'));

var podatkiSpomin = ["admin/nimda", "gost/gost"];


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Zunanja avtentikacija)
 */
app.get('/api/prijava', function(req, res) {
	// ...
	var user = req.query.uporabniskoIme;
	var pass = req.query.geslo;
	
	var rez = preveriSpomin(user, pass) || preveriDatotekaStreznik(user, pass);
	var napaka = "";
	
	if(user == null || pass == null) napaka = "Napačna zahteva.";
	if (!rez) napaka = "Avtentikacija ni uspela.";
	
	
	res.send({status: rez, napaka: napaka});
	// ...
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Prijava uporabnika v sistem)
 */
app.get('/prijava', function(req, res) {
	// ...
	
	
/*	var uporabniskoIme = req.query.uporabniskoIme;
	var geslo = req.query.geslo;
	var uspesno = preveriSpomin(uporabniskoIme, geslo) || preveriDatotekaStreznik(uporabniskoIme, geslo);

	var odgovor = "<html><head><title>" + (uspesno == true ? "Uspešno" : "Napaka") + "</title><style>body {padding:10px}</style></head>";
	odgovor += "<body>";
	if (uspesno)
		odgovor += "<p>Uporabnik <b>" + uporabniskoIme + "</b> uspešno prijavljen v sistem!</p>";
	else
		odgovor += "<p>Uporabnik <b>" + uporabniskoIme + "</b> nima pravice prijave v sistem!</p>";

	odgovor += "</body></html>";
	//console.log(odgovor);
	res.send(odgovor);*/
	
	var user = req.query.uporabniskoIme; //PREVERI CE DELA TUDI BREZ QUERY, ZAKAJ JE TA QUERY?
	var pass = req.query.geslo;
	var odgovor = "";
	
	var rez = preveriSpomin(user, pass) || preveriDatotekaStreznik(user, pass);
	var naslov = "";
	var status = "";
	if(rez){
		naslov = "Uspesno";
		status = " uspešno prijavljen v sistem!"
	} else {
		naslov = "Napaka";
		status = " nima pravice prijave v sistem!";
	}
	odgovor = "<html><head><title>"+ naslov + "</title><style>body {padding:10px}</style></head><body><p>Uporabnik <b>"+user+"</b>"+status+"</p></body></html>";
	console.log(odgovor);
	res.send(odgovor);
	// ...
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var podatkiSpomin = ["admin/nimda", "gost/gost"];


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (branje datoteka na strani strežnika)
 */
var podatkiDatotekaStreznik = JSON.parse(fs.readFileSync(__dirname + "/public/podatki/" + "uporabniki_streznik.json").toString());


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti
 */
function preveriSpomin(uporabniskoIme, geslo) {
	// ...
	//preverjanje spremenljivko
	var ret = false;
			
	for(var i = 0; i<podatkiSpomin.length; i++){
		var res = podatkiSpomin[i].split("/");
		if(res[0] === uporabniskoIme && res[1] === geslo) ret = true;
		//console.log(res[0]);
		//console.log(res[1]);
	}
	
	return ret;
	
}


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti
 */
function preveriDatotekaStreznik(uporabniskoIme, geslo) {
	// ...
	var ret = false;
				
	for(var i = 0; i<podatkiDatotekaStreznik.length; i++){
		var user = podatkiDatotekaStreznik[i]["uporabnik"];
		var pass = podatkiDatotekaStreznik[i]["geslo"];
		if(user === uporabniskoIme && pass === geslo) ret = true;
		//console.log(user);
		//console.log(pass);
	}
	
	return ret;
}